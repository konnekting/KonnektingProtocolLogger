/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package de.konnekting.konnektingprotocollogger;

import de.root1.slicknx.AutoDiscoverProgressListener;
import de.root1.slicknx.GroupAddressEvent;
import de.root1.slicknx.GroupAddressListener;
import de.root1.slicknx.Knx;
import de.root1.slicknx.KnxException;
import de.root1.slicknx.KnxInterfaceDevice;
import de.root1.slicknx.KnxInterfaceDeviceType;
import de.root1.slicknx.Utils;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author alexander
 */
public class App {
    
    private static final Logger LOG = LoggerFactory.getLogger(App.class);

    /**
     * returns upper case hex
     *
     * @param bytearray
     * @param whitespace
     * @return
     */
    public static String bytesToHex(byte[] bytearray, boolean whitespace) {
        StringBuilder sb = new StringBuilder(bytearray.length * 2);
        for (int i = 0; i < bytearray.length; i++) {
            sb.append(String.format(whitespace ? "%02x " : "%02x", bytearray[i] & 0xff));
        }
        return sb.toString().trim().toUpperCase();
    }

    public static void main(String[] args) throws KnxException {

        LOG.info("Detecting KNX interface devices...");
        List<KnxInterfaceDevice> discoverInterfaceDevices = Knx.discoverInterfaceDevices(3, new AutoDiscoverProgressListener() {
            @Override
            public void found(KnxInterfaceDevice device) {
                LOG.info("Found: "+device);
            }

            @Override
            public void noResult() {
                LOG.info("Found nothing");
            }

            @Override
            public void done(List<KnxInterfaceDevice> foundDevices) {
                LOG.info("Found:");
                for (KnxInterfaceDevice foundDevice : foundDevices) {
                    LOG.info("\t * "+foundDevice);
                }
            }
        });
        
        if (discoverInterfaceDevices.isEmpty()) {
            LOG.info("No devices detected! Exiting...");
            System.exit(1);
        }
        
        KnxInterfaceDevice device = null;
        
        for (KnxInterfaceDevice discoverInterfaceDevice : discoverInterfaceDevices) {
            if (discoverInterfaceDevice.getType()==KnxInterfaceDeviceType.ROUTING) {
                device = discoverInterfaceDevice;
                break;
            }
        }
        
        if (device == null) {
            LOG.info("No tunneling device found. Exiting.");
            System.exit(2);
        }
        
        LOG.info("Start monitoring!");
        Knx knx = Knx.getKnx(device);
        
        knx.addGroupAddressListener("15/7/255", new GroupAddressListener() {

            @Override
            public void readRequest(GroupAddressEvent event) {
                LOG.info("Unsupported: readRequest");
            }

            @Override
            public void readResponse(GroupAddressEvent event) {
                LOG.info("Unsupported: readResponse");
            }

            @Override
            public void write(GroupAddressEvent event) {
                String source = event.getSource();
                String destination = event.getDestination();
                byte[] data = event.getData();

                String details ="";
                
                byte version = data[0];
                byte msgtype = data[1];
                String msgTypeString = "";
                switch (msgtype) {
                    
                    case 0x00:
                        msgTypeString = "ACK";
                        details = data[2]==0x00?"ACK":"NACK";
                        details +=", Error: ";
                        switch(data[3]) {
                            case 0x00:
                                details+="OK";
                                break;
                            case 0x01:
                                details+="NOT SUPPORTED";
                                break;
                            case 0x02:
                                details+="DATA OPEN WRITE FAILED";
                                break; 
                            case 0x03:
                                details+="DATA OPEN READ FAILED";
                                break; 
                            case 0x04:
                                details+="DATA WRITE FAILED";
                                break; 
                            case 0x05:
                                details+="DATA READ FAILED";
                                break; 
                            case 0x06:
                                details+="DATA CRC FAILED";
                                break; 
                            case 0x07:
                                details+="TIMEOUT";
                                break; 
                            case 0x08:
                                details+="ILLEGAL STATE";
                                break; 
                            default:
                                details+=String.format("unknown, code=0x%02X", data[3]);
                        }
                        break;
                        
                    case 0x01:
                        msgTypeString = "PropPageRead";
                        details+="address="+(data[2]==0x00?"progmode":"IA match");
                        try {
                            details+=" IA="+Utils.getIndividualAddress(data[3], data[4]).toString();
                        } catch (KnxException ex) {
                            ex.printStackTrace();
                        }
                        details+=" pagenum="+String.format("0x%02X", data[5]);
                        break;

                    
                    case 0x02:
                        msgTypeString = "PropPageResp";
                        details += "n/a";
                        break;
                        
                    case 0x08:
                        msgTypeString = "Unload";
                        details+="factoryreset="+(data[2]==0x00?"0":"1");
                        details+=" IA="+(data[3]==0x00?"0":"1");
                        details+=" CO="+(data[4]==0x00?"0":"1");
                        details+=" param="+(data[5]==0x00?"0":"1");
                        details+=" data="+(data[6]==0x00?"0":"1");
                        break;
                        
                    case 0x09:
                        msgTypeString = "Restart";
                        try {
                            details+="IA="+Utils.getIndividualAddress(data[2], data[3]).toString();
                        } catch (KnxException ex) {
                            ex.printStackTrace();
                        }
                        break;
                        
                    case 0x0A:
                        msgTypeString = "ProgModeWrite";
                        try {
                            details+="IA="+Utils.getIndividualAddress(data[2], data[3]).toString();
                        } catch (KnxException ex) {
                            ex.printStackTrace();
                        }
                        details+=" mode="+(data[4]==0x00?"OFF":"ON");
                        break;
                        
                    case 0x0B:
                        msgTypeString = "ProgModeRead";
                        details += "n/a";
                        break;
                        
                    case 0x0C:
                        msgTypeString = "ProgModeResp";
                        try {
                            details+="IA="+Utils.getIndividualAddress(data[2], data[3]).toString();
                        } catch (KnxException ex) {
                            ex.printStackTrace();
                        }
                        break;
                        
                    case 0x1E:
                        msgTypeString = "MemWrite";
                        details+="count="+(int)(data[2]&0xff);
                        details+=" addr="+String.format("0x%04X",((
                                        + ((data[4] << 8) & 0x0000FF00)
                                        + ((data[3] << 0) & 0x000000FF))));
                        break;   
                        
                    case 0x1F:
                        msgTypeString = "MemRead";
                        details+="count="+(int)(data[2]&0xff);
                        details+=" addr="+String.format("0x%04X",((
                                        + ((data[4] << 8) & 0x0000FF00)
                                        + ((data[3] << 0) & 0x000000FF))));
                        break;                       
                    case 0x20:
                        msgTypeString = "MemResp";
                        break;
                    case 0x28:
                        msgTypeString = "DataWritePrep";
                        details+="type=";
                        switch(data[2]) {
                            case 0x00:
                                details+="UPDATE";
                                break;
                            case 0x01:
                                details+="DATA";
                                break;
                            default:
                                details+=String.format("UNKNOWN:0x%02x", data[2]);
                        }
                        break;
                    case 0x29:
                        msgTypeString = "DataWrite";
                        details+="count="+(int)(data[2]&0xff);
                        break;
                    case 0x2A:
                        msgTypeString = "DataWriteFinish";
                        details+="crc="+((((data[2] << 24) & 0xFF000000)
                                        + ((data[3] << 16) & 0x00FF0000)
                                        + ((data[4] << 8) & 0x0000FF00)
                                        + ((data[5] << 0) & 0x000000FF)));
                        break;
                    case 0x2B:
                        msgTypeString = "DataRead";
                        details+="type=";
                        switch(data[2]) {
                            case 0x00:
                                details+="UPDATE";
                                break;
                            case 0x01:
                                details+="DATA";
                                break;
                            default:
                                details+=String.format("UNKNOWN:0x%02x", data[2]);
                        }
                        details+=String.format(" id=0x%02x", data[3]);
                                
                        break;
                    case 0x2C:
                        msgTypeString = "DataReadResp";
                        details+="datatype="+String.format("0x%02X", data[2]);
                        details+=" dataID="+String.format("0x%02X", data[3]);
                        details+=" size="+((((data[4] << 24) & 0xFF000000)
                                        + ((data[5] << 16) & 0x00FF0000)
                                        + ((data[6] << 8) & 0x0000FF00)
                                        + ((data[7] << 0) & 0x000000FF)));
                        details+=" crc="+((((data[8] << 24) & 0xFF000000)
                                        + ((data[9] << 16) & 0x00FF0000)
                                        + ((data[10] << 8) & 0x0000FF00)
                                        + ((data[11] << 0) & 0x000000FF)));                        
                        break;
                    case 0x2D:
                        msgTypeString = "DataReadData";
                        details+="count="+(int)(data[2]&0xff);
                        break;
                    case 0x2E:
                        msgTypeString = "DataRemove";
                        switch(data[2]) {
                            case 0x00:
                                details+="UPDATE (INVALID!!!)";
                                break;
                            case 0x01:
                                details+="DATA";
                                break;
                            default:
                                details+=String.format("type=UNKNOWN:0x%02x", data[2]);
                        }
                        details+=String.format(" id=0x%02x", data[3]);
                        break;                                                                                    
                    default:
                        msgTypeString = String.format("0x%02X", msgtype);
                }

                LOG.info(String.format("%8s > %-8s   v%02X   msgType=0x%02X %-15s   [ %s ]  %s", source, destination, version, msgtype, msgTypeString, bytesToHex(Arrays.copyOfRange(data, 2, data.length), true), details));

            }
        });
        
        while(true) {
            try {
                Thread.currentThread().sleep(1000);
            } catch (InterruptedException ex) {
            }
        }

    }

}
